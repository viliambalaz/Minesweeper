"""
    Minesweeper.py
    Viliam Balaz
    21.10.2017    v 1.1.0
"""
import os, random, itertools
from Tkinter import *
from PIL import Image, ImageTk
import mines_stopwatch
root = Tk()
home_directory = os.path.dirname(os.path.abspath(__file__))
#todo: by moving to Settings/Highscores solve the problems

def get_image(path):
    'open image, edit it to one unique size and return it'
    img = Image.open(path)
    img = img.resize((22,22), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    return img


def load_imag():
    'returns dictionary png, where are stored icons'
    png ={}
    path = home_directory + '/icons'
    files = os.listdir(path)
    for f in files:
        if f.endswith('.png'):
            name = f[:-4]  # -4 index of dot
            png[name] = get_image(path + '/' + f)
    return png


png = load_imag()


class Application(Frame):

    def __init__(self, master=None, **kw):
        Frame.__init__(self, master, kw)
        #create_menu---------------------------
        self.mainMenu = Menu(self)
        self.master.config(menu=self.mainMenu)
        self.toolbar = Frame(self)
        self.gameplan = Frame(self)  # main frame for all mines
        self.timer = mines_stopwatch.StopWatch(self.toolbar)
        self.miner = Label(self.toolbar, text='000')
        self.smile = Button(self.toolbar, image=png['sg'], command=self.smile_work)
        self.pause = Button(self.toolbar, text='Pause', width=4, command=self.pause_work)
        self.setti_app = Settings(self)
        self.high_app = Highscore(self)
        self.hint = Button(self.toolbar, text='hint', command=self.hint_work)
        self.create_menu()

        #create_gameplan---------------------
        ln, cu, per = self.setti_app.read_settings()
        self.Mfield = Minefield(ln, cu, per)
        self.layout = [[None for j in range(self.Mfield.columns)] for i in range(self.Mfield.lines)]

        self.pause_freeze = 1 #cant use pause button
        self.is_paused = 1
        self.mine_count = 0
        self.counter = self.Mfield.numbers
    #------__init__------------------------------------------------

    def run(self):
        'open the window'
        self.mainloop()

    def create_menu(self):
        'creates menu and toolbar; automatic initialization'
        #Menu------------------------------
        #todo: comple the pop-up menu; add widgets and work function to them
        fileMenu = Menu(self.mainMenu)
        self.mainMenu.add_cascade(label='File', menu=fileMenu)
        fileMenu.add_command(label='Settings', command=self.open_settings)
        fileMenu.add_command(label='Highscore', command=self.open_highscore)
        fileMenu.add_command(label='Quit', command=lambda:self.quit())

        editMenu = Menu(self.mainMenu)
        self.mainMenu.add_cascade(label='Edit', menu=editMenu)
        editMenu.add_command(label='command_edit')

        #Toolbar-----------------------------------------------
        self.pause.pack(side=RIGHT, padx=3)
        self.timer.pack(side=RIGHT, padx=3)
        self.miner.pack(side=RIGHT, padx=3)
        self.hint.pack(side=RIGHT, padx=3)
        self.smile.pack()
        self.toolbar.pack(side=TOP, fill=X)
    #-----------create_menu----------------------------

    def create_gameplan(self):
        """
        'function create minefield, buttons for each mine and push it to application"
        size: lines * columns
        mine - Button represents mine
        line - tmp frame for mines in one line
        field[l][c] - 2D tmp list with how many mines around
        self.layout[l][c] - 2D list with ID of Mine_Button
        """
        self.gameplan = Frame(self)  # main frame for all mines
        for i in range(self.Mfield.lines):
            line_frm = Frame(self.gameplan)
            for j in range(self.Mfield.columns):
                mine = Mine_Button(line_frm, i, j)
                self.layout[i][j] = mine
                mine.pack( side=LEFT)
                mine.bind('<Button-1>', mine.left_click)
                mine.bind('<Button-3>', mine.right_click)
                mine.bind('<Button-2>', mine.middle_click)
            line_frm.pack()
        self.gameplan.pack()
    #-----------crete_gameplan----------------------------

    def smile_work(self):
        """
        #work function for button smiley
        set image of smile
        create new minefield
        reset stopwatch
         ...
        """
        self.setti_app.pack_forget()
        self.high_app.pack_forget()
        self.gameplan.destroy()
        self.smile.config(image=png['sg'])

        ln, cn, per = self.setti_app.read_settings()
        self.Mfield = Minefield(ln, cn, per)
        self.layout = [[None for j in range(self.Mfield.columns)] for i in range(self.Mfield.lines)]
        self.create_gameplan()

        self.mine_count = self.Mfield.num_mi
        self.miner.config(text=self.mine_count)
        self.counter = self.Mfield.numbers
        self.timer.Reset()
    #-----------smile_work----------------------------

    def inc_miner(self):
        self.mine_count += 1
        self.miner.config(text=self.mine_count)

    def dec_miner(self):
        self.mine_count -= 1
        self.miner.config(text=self.mine_count)

    def pause_work(self):
        'work function for button pause'
        if self.pause_freeze == 1:
            return

        if self.is_paused == 1:
            self.pause.config(text='Pause')
            self.is_paused = 0
            self.timer.Start()
            self.gameplan.pack()
        elif self.is_paused == 0:
            self.is_paused = 1
            self.pause.config(text='Resume')
            self.timer.Stop()
            self.gameplan.pack_forget()
    #-----------pause_work----------------------------

    def hint_work(self):
        'work function for button hint'
        hint = False
        for i in range(len(self.layout)):
            for j in range(len(self.layout[0])):
                mine = self.layout[i][j]
                if mine.around == 0: continue  #there is auto null
                if mine.place == 1:
                    flag =   [mine_n for mine_n in mine.neighbours if mine_n.place == 2]
                    hidden = [mine_n for mine_n in mine.neighbours if mine_n.place == 0]
                    
                    if len(flag) == mine.around and len(hidden):
                        hint += 1
                        for m in hidden:
                            m.left_click(None)
                            #return #show only one
                        continue

                    if len(hidden) + len(flag) == mine.around and len(hidden):
                        hint = True
                        for m in hidden:
                            m.right_click(None)
                            #return #show only one
                        continue
        if not hint:
            self.hint2_work()
    #-----------hint_work----------------------------

    def hint2_work(self):
        'method wit q_marks'
        for i in range(len(self.layout)):
            for j in range(len(self.layout[0])):
                mine = self.layout[i][j]
                if mine.around == 0 or mine.place != 1: continue
                flag = [mine_n for mine_n in mine.neighbours if mine_n.place == 2]
                hidden = [mine_n for mine_n in mine.neighbours if mine_n.place == 0]

                if len(hidden) == 2 and mine.around - len(flag) == 1:   #2 hidden and 1 mine left
                    #print 'i,j: %d %d around: %d len(flag): %d len(hidden): %d' % (i, j, mine.around, len(flag), len(hidden))

                    for m in hidden:
                        m.middle_click(None) # push q_marks

                    neigh = [mine_n for mine_n in mine.neighbours if mine.place ==1]
                    self.hint3_work(neigh)

                    for m in hidden:
                        m.middle_click(None) # remove_qmarks
    #-----------hint2_work----------------------------

    def hint3_work(self, neighbours):
        'search mines and numbers with q_marks; tmp function for hint work2'
        for mine in neighbours:
            flag = [mine_n for mine_n in mine.neighbours if mine_n.place == 2]
            hidden = [mine_n for mine_n in mine.neighbours if mine_n.place == 0]
            q_marks = [mine_n for mine_n in mine.neighbours if mine_n.place == 3]

            if len(q_marks) != 2 : continue

            if (len(flag) +1 == mine.around) and len(hidden):  # +1 -> there are 2 q_marks -> one mine
                #print "!!! left_clicking"
                for m in hidden: m.left_click(None)

            if (len(hidden) + len(flag)+1 == mine.around) and len(hidden):
                #print "!!! right_clicking"
                for m in hidden: m.right_click(None)
    #-----------hint3_work----------------------------

    def open_settings(self):
        'work function for fileMenu -> Settings'
        self.gameplan.destroy()
        self.high_app.pack_forget()
        self.setti_app.pack()

    def open_highscore(self):
        self.high_app.destroy()
        self.high_app = Highscore(self)
        self.gameplan.destroy()
        self.setti_app.pack_forget()
        self.high_app.pack()
    
#----------------Application----------------------------------------------------------------------------------------


class Mine_Button(Button):
    """
    app: global name for main application
    around: how many mines are around; 0-8, 9 mine
    place: 0-nothing, 1-showing number, 2-flag, 4-end of game
    neighbours: list with id of buttons
    left_click: set 1, unable when 1,2
    right_click: set between 0-2-3, unable when 1
    *>> take a look at petri net <<*
    """

    def __init__(self, master, x, y, **kwargs):
        Button.__init__(self, master, **kwargs)
        self.around = -1    #we dont now data by initialization
        self.place = 0     #forbiden clicking right button before first_click
        self.x = x
        self.y = y
        self.neighbours = []

    def mineButton_init(self, i,j):
        'assign values to  all self.Mine_Button'
        field = app.Mfield.mines_init(i, j) #2D list stored-> self.value
        border = lambda i, j: (0 <= i < app.Mfield.lines) and (0 <= j < app.Mfield.columns)
        direction = [x for x in itertools.product([-1, 0, 1], [-1, 0, 1])]
        direction.remove((0, 0))

        for i in range(len(field)):
            for j in range(len(field[0])):
                app.layout[i][j].around = field[i][j]
                #app.layout[i][j].place = 0
                for a, b in direction: #init neighbours
                    if border(i+a, j+b):
                        app.layout[i][j].neighbours.append(app.layout[i+a][j+b])
    #-----------mineButton_init----------------------------

    def left_click(self, event, zero=0):
        'work function for Mine_Button <Button-1>'
        #todo: execute left_click when the cursor is inside button

        #cant provide in this cases
        if self.place in [1,4]:
            return
        if self.place == 2 and zero == 0:
            return

        # if first click - initialization values to buttons
        if app.counter == app.Mfield.numbers:
            self.mineButton_init(self.x, self.y)
            app.timer.Start() #turn on stopwatch
            app.pause_freeze = 0
            app.is_paused = 0
            app.pause.config(text='Pause')

        if self.around != 9:
            app.counter -=1
            if app.counter == 0:
                print 'you win'
                app.smile.config(image=png['sw'])
                self.game_over()
                app.high_app.write_highscore(winn=True)
            self.config(image=png[`self.around`])
            self.place =1
            if self.around == 0:    #auto null
                for id in self.neighbours:
                    id.left_click(None, zero=1)
        elif self.around == 9:
            print 'you loose'
            app.smile.config(image=png['sl'])
            self.game_over()
            app.high_app.write_highscore(winn=False)
    #-----------left_click---------------------------

    def right_click(self, event):
        if self.place in [1,4]:
            return
        elif self.place == 0:
            self.config(image=png['f'])
            self.place = 2
            app.dec_miner()
        elif self.place in [2]:
            self.config(image='') #any image
            self.place = 0
            app.inc_miner()
    #-----------right_click----------------------------

    def middle_click(self, event):
        """ for  '?' """
        if self.place == 0:
            self.place = 3
            self.config(image=png['q'])
        elif self.place == 3:
            self.place = 0
            self.config(image='')
    #-----------middle_click----------------------------

    def game_over(self):
        app.timer.Stop()
        app.pause_freeze = 1
        for i in range(len(app.layout)):
            for j in range(len(app.layout[0])):
                id = app.layout[i][j]
                if id.around == 9:
                    # set different images
                    if id.place == 2:
                        id.config(image=png['9f'])  # good placed flag
                    elif id.place in [0, 3]:
                        id.config(image=png['9'])
                elif (id.around != 9) and (id.place == 2):  # player set flag where is any mine
                    id.config(image=png['fr'])
                id.place = 4  # freeze buttons
        self.config(image=png['9r'])  # mine, that you cliced
    #-----------game_over----------------------------

#--------Mine_Button-------------------------------------------


class Minefield(object):

    def __init__(self, lines, columns, percent):
        'size: lines*columns ; percentage of mines; number of numbers'
        self.lines = lines
        self.columns = columns
        self.num_mi = int(lines*columns*percent/100.0)
        self.numbers = self.lines * self.columns - self.num_mi

    def generate_mines(self, i, j):
        all_coordinates = [x for x in itertools.product(range(self.lines), range(self.columns))]
        all_coordinates.remove((i,j)) #dont place mine on first click
        return [x for x in random.sample(all_coordinates, self.num_mi)]

    def mines_init(self, i, j):
        'returns list[l][c], stores number how many mines are around'
        field = [[[0, False] for jj in range(self.columns)] for ii in range(self.lines)]
        where = self.generate_mines(i,j)
        for w in where:
            field[w[0]][w[1]][1] = True   #here is mine

        border = lambda i,j: (0<= i <self.lines) and (0 <= j < self.columns)
        direction = [x for x in itertools.product([-1, 0, 1], [-1, 0, 1])]
        direction.remove((0, 0))

        for i in range(len(field)):
            for j in range(len(field[0])):
                if field[i][j][1] == True:    #if mine assign value 9
                    field[i][j][0] = 9
                    continue
                for a,b in direction:
                    if border(i+a,j+b) and field[i+a][j+b][1]:
                        field[i][j][0] += 1

        for i in range(len(field)):    # only numbers
            field[i] = [x[0] for x in field[i]]
        return field
    #-----------mines_init---------------------------------------------

#-------Plan-------------------------------------------------------------


class Settings(Frame):
    'application for Settings'

    def __init__(self, master=None, **kw):
        Frame.__init__(self, master, kw)
        self.v = IntVar()
        self.values = {1: '10\n10\n10\n', 2: '16\n16\n16\n', 3: '16\n30\n21\n', 4: ''}
        self.makeWidget()
        self.path = home_directory + '/settings'

    def makeWidget(self):
        radio1 = Radiobutton(self, text='easy 10x10 \t 10 mines', variable=self.v, value=1, command=self.radio_work)
        radio2 = Radiobutton(self, text='medium 16x16 \t 40 mines', variable=self.v, value=2, command=self.radio_work)
        radio3 = Radiobutton(self, text='hard 16x30 \t 100 mines', variable=self.v, value=3, command=self.radio_work)
        radio4 = Radiobutton(self, text='custom', variable=self.v, value=4, command=self.radio_work)
        radio1.pack()
        radio2.pack()
        radio3.pack()
        radio4.pack()

        tmp_frm = Frame(self)
        self.app1 = Custom_Settings_Frame(tmp_frm, 4, 20)
        self.app2 = Custom_Settings_Frame(tmp_frm, 4, 30)
        self.app3 = Custom_Settings_Frame(tmp_frm, 4, 98)
        self.app1.pack(side=LEFT)
        self.app2.pack(side=LEFT)
        self.app3.pack(side=LEFT)
        tmp_frm.pack()

        save = Button(self, text='save', command=self.save_work)
        save.pack(side=RIGHT)
    #-----------makeWidget---------------------------------------------

    def radio_work(self):
        print 'self.v:', self.v.get()

    def save_work(self):
        'command function for button save, write option to settings'
        if self.v.get() == 0:
            print 'Any option selected.'
            return

        custom_value = str(self.app1.lbl.cget('text')) + '\n'
        custom_value += str(self.app2.lbl.cget('text')) +'\n'
        custom_value += str(self.app3.lbl.cget('text')) +'\n'
        self.values[4] = custom_value
        print self.values[self.v.get()]
        file = open(self.path, 'w')
        file.write(self.values[self.v.get()])
        file.close()
        self.pack_forget()
    #-----------save_work---------------------------------------------

    def read_settings(self):
        'function read from settings and return contens'
        try:
            file = open(self.path, 'r')
        except IOError:
            file = open(self.path, 'w')
            file.write('10\n10\n10\n')
            file.close()
            file = open(self.path, 'r')
        ln = int(file.readline())
        cn = int(file.readline())
        per = int(file.readline())
        file.close()
        return (ln,cn,per)

#-----------Settings---------------------------------------------

class Custom_Settings_Frame(Frame):
    """
        tmp class for Settings
        makes frame for custom option
    """
    def __init__(self,master, limit_d, limit_u, **kw):
        Frame.__init__(self,master, kw)

        self.limit_d = limit_d
        self.limit_u = limit_u
        self.lbl = Label(self, text=str(self.limit_d))
        self.lbl.pack(side=LEFT)

        self.is_pressed = False
        but_plus = Button(self, text='+')
        but_plus.bind('<Button-1>', self.click)
        but_plus.bind('<ButtonRelease-1>', self.release)
        but_plus.pack(side=LEFT)

        but_minus = Button(self, text='-')
        but_minus.bind('<Button-1>', self.click)
        but_minus.bind('<ButtonRelease-1>', self.release)
        but_minus.pack(side=LEFT)
    #-----------__init__---------------------------------------------

    def click(self, event):
        self.is_pressed = True
        self.inc = 1 if event.widget.cget('text')=='+' else -1
        print 'button text: ', event.widget.cget('text')
        self.pool()

    def release(self, event):
        print 'Canceling....'
        self.after_cancel(self.after_id)

    def pool(self):
        if self.is_pressed:
            self.work()
            self.after_id= self.after(100, self.pool)

    def work(self):
        value = int(self.lbl.cget('text')) + self.inc
        if value > self.limit_u or value < self.limit_d:
            value -= self.inc
        self.lbl.config(text=value)

#-----------Custom_Settings_Frame---------------------------------------------


class Highscore(Frame):
    """
    application for Highscore
    """
    def __init__(self, master=None, **kw):
        Frame.__init__(self, master, kw)
        self.path = home_directory + '/highscore'
        self.makeWidget()

    def read_high(self):
        try: file =open(self.path, 'r')
        except IOError:
            file = open(self.path, 'w+')
            for i in range(19): file.write('0 ')
            file.seek(0)

        data = file.readline()
        data = data.strip()
        data = [int(x) for x in data.split(' ')]
        return data

    def makeWidget(self):
        data = self.read_high()
        #total
        lbl_text =''
        lbl_text+='total games: '+str(data[0]) +'\n'
        lbl_text+='win: '+str(data[1]) + '\n'
        lbl_text+='loose: '+str(data[2]) +'\n'
        lbl_text+='winnrate: '+str(data[3])+ '%' +'\n\n'

        mode = ['Easy', 'Normal', 'Hard']
        for m in range(len(mode)):
            jump = (m * 5) + 4

            lbl_text+= mode[m] +' games: ' + str(data[0+jump]) +'\n'
            lbl_text+='win: ' + str(data[1+jump]) + '\n'
            lbl_text+='loose: ' + str(data[2+jump]) + '\n'
            lbl_text+='winnrate: ' + str(data[3+jump]) + '%\n'
            lbl_text+='best_time: ' + str(data[4+jump]) + '\n\n'

        lbl = Label(self, text=lbl_text).pack()
        reset_buton = Button(self, text='reset', command=self.reset).pack()
    #-----------makeWidget---------------------------------------------

    #todo: dynamic change highscores
    def write_highscore(self, winn):
        'function write information about current played game to highscore'
        #find out the mode
        mode = -1
        if (app.Mfield.lines==10) and (app.Mfield.columns==10) and (app.Mfield.num_mi==10):
            mode = 1
        elif (app.Mfield.lines==16) and (app.Mfield.columns==16) and (app.Mfield.num_mi==40):
            mode = 2
        elif (app.Mfield.lines==16) and (app.Mfield.columns==30) and (app.Mfield.num_mi==100):
            mode = 3
        else:
            return #dont write statistics on custom mode

        data = self.read_high()
        data[0] += 1  # total games
        data[1] += winn # total win
        data[2] += not winn # total lose
        data[3] = int((data[1] * 100.0) / data[0])   # winrate % data[3]

        jump = ((mode-1) *5) + 4
        data[0+jump] += 1  # total games mode
        data[1+jump] +=winn  # total win-mode
        data[2+jump] += not winn  # total lose-mode
        data[3+jump]= int((data[1+jump] * 100.0) / data[0+jump])  # winrate-mode % data[3]

        num = int(data[4+jump])
        time = app.timer._elapsedtime
        if (winn == True) and ((time < num) or (num == 0)):
            data[4+jump] = num

        file = open(self.path, 'w')
        data = [str(x) for x in data]
        file.write(' '.join(data))
        file.close()
    #-------write_highscore--------------------------------

    def reset(self):
        'work function for button reset'
        print 'doing reset'
        file = open(self.path, 'w')
        for i in range(19): file.write('0 ')
        file.close()
        self.destroy()
        app.open_highscore()

#-----------Highscore---------------------------------------------


app = Application(root)
app.pack()
app.run()
