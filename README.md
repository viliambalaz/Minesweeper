Minesweeper is a single-player puzzle video game. The objective of the game is to clear a rectangular board containing hidden "mines" or bombs without detonating any of them, with help from clues about the number of neighboring mines in each field. [wiki](https://en.wikipedia.org/wiki/Minesweeper_(video_game))

## Run
- version: python 2.7.12
- external libraries: Tkinter, PIL
- `python Minesweeper_Tk.py`
